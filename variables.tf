variable "name" {
  default     = ""
  type        = string
  description = "Name for SQS queue"
}

variable "delay_seconds" {
  default     = ""
  type        = string
  description = "Delay Seconds for RDS SQS queue"
}

variable "max_message_size" {
  default     = ""
  type        = string
  description = "Max Message Size for SQS queue"
}

variable "message_retention_seconds" {
  default     = ""
  type        = string
  description = "Message Retention Seconds for SQS queue"
}

variable "receive_wait_time_seconds" {
  default     = ""
  type        = string
  description = "Receive Wait Time Seconds for SQS queue"
}