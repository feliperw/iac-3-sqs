# IAC 3 - SQS



## Objetivo:
- Criar um modulo terraform que chame outros 3 modulos:
    - Criaçao de EC2
    - Criaçao de um RDS
    - Criaçao de uma fila SQS

- Cada modulo deve receber pelo menos 3 variaveis e fazer pelo menos 1 output
- Todos os modulos precisam estar dispoveis no Git - modulo remoto
- Chamada para os modulos precisam ser via GIT

## Pré Reqs:
- Instalar o Terraform (https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli) na maquina onde será orquestrado o provisionamento da infra
- Ter um usuario programatico na AWS (https://docs.aws.amazon.com/pt_br/IAM/latest/UserGuide/id_users_create.html)

## Execucão:
- Baixar os arquivos deste repositório:
```
git clone https://gitlab.com/feliperw/iac-3-sqs.git
```

- Exporte as variáveis de ambiente para acesso a AWS:
```
export AWS_ACCESS_KEY_ID="{Insira o ID do usuário programático da AWS criado nos passos anteriores}"
export AWS_SECRET_ACCESS_KEY="{Insira a Secret do usuário programático da AWS criado nos passos anteriores}"
```

- Exporte as variáveis de ambiente referente a configuração da VM:
```
export TF_VAR_name="{Nome da fila}"
export TF_VAR_delay_seconds="{Tempo de Delay}"
export TF_VAR_max_message_size="{Tamanho maximo da mensagem}"
export TF_VAR_message_retention_seconds="{Periodo de retenção}"
export TF_VAR_receive_wait_time_seconds="{Periodo de recebimento}"
```
- Execute os comandos do Terraform para criar a infra:
```
terraform init
terraform plan
terraform apply
```

- Após a criação da infra, será exibido o IP de cada VM, endereço de DNS do LB e endpoint do RDS

```
sqs_endpoint = ""
```
